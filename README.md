# plot_ITPC.jl

NeuroAnalyzer plugin: plot ITPC.

This software is licensed under [The 2-Clause BSD License](LICENSE).

## Usage

```julia
p = plot_itpc(eeg, ch="Fp1", t=1028)
plot_save(p, file_name="itpc.png")
```

![](itpc.png)