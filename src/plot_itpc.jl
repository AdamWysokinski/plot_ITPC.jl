export plot_itpc
export plot_itpc_s
export plot_itpc_f

"""
    plot_itpc(obj; <keyword arguments>)

Plot ITPC (Inter-Trial-Phase Clustering).

# Arguments

- `obj:NeuroAnalyzer.NEURO`
- `ch::String`: channel name
- `t::Int64`: time point to plot
- `z::Bool=false`: plot ITPCz instead of ITPC
- `w::Union{AbstractVector, Nothing}=nothing`: optional vector of epochs/trials weights for wITPC calculation
- `mono::Bool=false`: use color or grey palette
- `kwargs`: optional arguments for plot() function

# Returns

- `p::Plots.Plot{Plots.GRBackend}`
"""
function plot_itpc(obj::NeuroAnalyzer.NEURO; ch::String, t::Int64, z::Bool=false, w::Union{AbstractVector, Nothing}=nothing, mono::Bool=false, kwargs...)::Plots.Plot{Plots.GRBackend}

    itpc_value, itpcz_value, itpc_angle, itpc_phases = itpc(obj, ch=ch, t=t, w=w)
    itpc_value = round(itpc_value[1], digits=2)
    itpcz_value = round(itpcz_value[1], digits=2)
    itpc_angle = round(itpc_angle[1], digits=2)
    itpc_phases = vec(itpc_phases)

    t = s2t(obj, s=t)
   
    p1 = plot_histogram(itpc_phases,
                        xticks=[-3.14, 0.0, 3.14],
                        title="Phase angles across trials\nchannel: $ch",
                        xlabel="Phase angle [rad]",
                        ylabel="Count/bin",
                        legend=false)

    if z == false
        if w === nothing
            p2 = plot_polar(itpc_phases, m=(itpc_angle, itpc_value), mono=mono, title="Phase differences\nITPC at $t s = $itpc_value")
        else
            p2 = plot_polar(itpc_phases, m=(itpc_angle, itpc_value), mono=mono, title="Phase differences\nwITPC at $t s = $itpc_value")
        end
    else
        itpcz_value > 1 && (itpcz_value = 1)
        if w === nothing
            p2 = plot_polar(itpc_phases, m=(itpc_angle, itpcz_value), mono=mono, title="Phase differences\nITPCz at $t s = $itpcz_value")
        else
            p2 = plot_polar(itpc_phases, m=(itpc_angle, itpcz_value), mono=mono, title="Phase differences\nwITPCz at $t s = $itpcz_value")
        end
    end
 
    p = plot_compose([p1, p2], layout=(1, 2), kwargs...)

    return p

end

"""
    plot_itpc_s(obj; <keyword arguments>)

Plot spectrogram of ITPC (Inter-Trial-Phase Clustering).

# Arguments

- `obj::NeuroAnalyzer.NEURO`
- `ch::String`: channel name
- `frq_lim::Tuple{Real, Real}`: frequency bounds for the spectrogram
- `frq_n::Int64=length(frq_lim[1]:frq_lim[end])`: number of frequencies
- `frq::Symbol=:lin`: linear (:lin) or logarithmic (:log) frequencies
- `z::Bool=false`: plot ITPCz instead of ITPC
- `w::Union{AbstractVector, Nothing}=nothing`: optional vector of epochs/trials weights for wITPC calculation
- `xlabel::String="Time [s]"`: x-axis label
- `ylabel::String="Frequency [Hz]"`: y-axis label
- `title::String="ITPC spectrogram"`: plot title
- `mono::Bool=false`: use color or grey palette
- `kwargs`: optional arguments for plot() function

# Returns

- `p::Plots.Plot{Plots.GRBackend}`
"""
function plot_itpc_s(obj::NeuroAnalyzer.NEURO; ch::String, frq_lim::Tuple{Real, Real}, frq_n::Int64=length(frq_lim[1]:frq_lim[end]), frq::Symbol=:lin, z::Bool=false, w::Union{AbstractVector, Nothing}=nothing, xlabel::String="Time [s]", ylabel::String="Frequency [Hz]", title::String="default", mono::Bool=false, kwargs...)::Plots.Plot{Plots.GRBackend}

    pal = mono == true ? :grays : :darktest
    title == "default" && (title = "ITPC spectrogram\nchannel: $ch")
    
    itpc_s, itpc_z_s, f = itpc_spec(obj, ch=ch, frq_lim=frq_lim, frq_n=frq_n, frq=frq, w=w)

    s = z == true ? itpc_z_s : itpc_s

    p = plot_spectrogram(obj,
                         s,
                         c_idx=1,
                         frq_lim=(f[1], f[end]),
                         title=title,
                         xlabel=xlabel,
                         ylabel=ylabel;
                         kwargs...)

    return p

end

"""
    plot_itpc_f(obj; <keyword arguments>)

Plot time-frequency plot of ITPC (Inter-Trial-Phase Clustering).

# Arguments

- `obj::NeuroAnalyzer.NEURO`
- `ch::String`: channel name
- `f::Real`: frequency to plot
- `frq_lim::Tuple{Real, Real}`: frequency bounds for the spectrogram
- `frq_n::Int64=length(frq_lim[1]:frq_lim[end])`: number of frequencies
- `frq::Symbol=:lin`: linear (:lin) or logarithmic (:log) frequencies
- `z::Bool=false`: plot ITPCz instead of ITPC
- `w::Union{AbstractVector, Nothing}=nothing`: optional vector of epochs/trials weights for wITPC calculation
- `xlabel::String="Time [s]"`: x-axis label
- `ylabel::String="Frequency [Hz]"`: y-axis label
- `title::String="default"`: plot title
- `mono::Bool=false`: use color or grey palette
- `kwargs`: optional arguments for plot() function

# Returns

- `p::Plots.Plot{Plots.GRBackend}`
"""
function plot_itpc_f(obj::NeuroAnalyzer.NEURO; ch::String, frq_lim::Tuple{Real, Real}, frq_n::Int64=length(frq_lim[1]:frq_lim[end]), frq::Symbol=:lin, f::Real, z::Bool=false, w::Union{AbstractVector, Nothing}=nothing, xlabel::String="Time [s]", ylabel::String="ITPC", title::String="default", mono::Bool=false, kwargs...)::Plots.Plot{Plots.GRBackend}

    @assert f > 0 "f must be ≥ 0."
    @assert f ≤ sr(obj) ÷ 2 "f must be ≤ $(sr(obj) ÷ 2)."
    @assert f >= frq_lim[1] "f must be ≥ $(frq_lim[1])."
    @assert f <= frq_lim[2] "f must be ≤ $(frq_lim[2])."

    itpc_s, itpc_z_s, itpc_f = itpc_spec(obj, ch=ch, frq_lim=frq_lim, frq_n=frq_n, frq=frq, w=w)
    title == "default" && (title = "ITPC at frequency $f Hz\nchannel: $ch")

    s = z == true ? itpc_z_s[vsearch(f, itpc_f), :] : itpc_s[vsearch(f, itpc_f), :]

    p = plot(obj,
             reshape(s, 1, :, 1),
             c_idx=1,
             ep=1,
             title=title,
             xlabel=xlabel,
             ylabel=ylabel,
             mono=mono;
             kwargs...)

    return p

end
